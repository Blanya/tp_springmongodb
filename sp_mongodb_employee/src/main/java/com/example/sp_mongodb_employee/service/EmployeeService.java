package com.example.sp_mongodb_employee.service;

import com.example.sp_mongodb_employee.dto.EmployeeResponseDTO;
import com.example.sp_mongodb_employee.entity.Employee;
import com.example.sp_mongodb_employee.repository.EmployeeRepository;
import com.example.sp_mongodb_employee.tools.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository _employeeRepository;

    @Autowired
    private Mapper _mapper;

    public Employee createEmployee(Employee employee) throws Exception {
        if(!employee.getFirstName().isBlank() && !employee.getLastName().isBlank() && employee.getSalary()>0 && employee.getStartingDate() != null && employee.getPoste() != null){
            //Generer id UUID
            UUID uuid = UUID.randomUUID();
            employee.setId(uuid.toString());

            return _employeeRepository.save(employee);
        }
        else throw new Exception("Tous les champs doivent être remplis");
    }

    public EmployeeResponseDTO getEmployeeById(String id) throws Exception {
        try{
            return _mapper.fromEmployee(_employeeRepository.findById(id).get());
        }catch (Exception e)
        {
            throw new Exception("Aucun employé trouvé avec cet id");
        }
    }

    public List<EmployeeResponseDTO> getAllEmployees(){
        List<EmployeeResponseDTO> responseDTOList = new ArrayList<>();
        List<Employee> employees = _employeeRepository.findAll();

        for (Employee e: employees){
            responseDTOList.add(_mapper.fromEmployee(e));
        }
        return responseDTOList;
    }

    public Employee updateEmployee(Employee employee) throws Exception {
        try {
            getEmployeeById(employee.getId());
            return _employeeRepository.save(employee);
        }catch (Exception e){
            throw new Exception("L'employe que vous souhaitez modifier n'existe pas");
        }
    }

    public String deleteEmployee(String id) throws Exception {
        try{
            _employeeRepository.deleteById(id);
            return "Employé supprimé des archives";
        }catch (Exception e){
            throw new Exception("Erreur lors de la suppression");
        }

    }

    public String deleteEmployee(List<String> ids) throws Exception {
        try{
            _employeeRepository.deleteAllByIdIn(ids);
            return "Les employés ont bien été supprimé du registre";
        }catch (Exception e){
            throw new Exception("Erreur lors de la suppression");
        }
    }
}
