package com.example.sp_mongodb_employee.controller;

import com.example.sp_mongodb_employee.dto.EmployeeResponseDTO;
import com.example.sp_mongodb_employee.entity.Employee;
import com.example.sp_mongodb_employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
@CrossOrigin(origins = "http://localhost:4200/", methods = {RequestMethod.DELETE, RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT})
public class EmployeeController {
    @Autowired
    private EmployeeService _employeeService;

    @PostMapping("")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) throws Exception {
        return ResponseEntity.ok(_employeeService.createEmployee(employee));
    }

    @GetMapping("")
    public ResponseEntity<List<EmployeeResponseDTO>> getAll() {
        return ResponseEntity.ok(_employeeService.getAllEmployees());
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeResponseDTO> getEmployeeById(@PathVariable String id) throws Exception {
        return ResponseEntity.ok(_employeeService.getEmployeeById(id));
    }

    @PutMapping("")
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee) throws Exception {
        return ResponseEntity.ok(_employeeService.updateEmployee(employee));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteEmployeeById(@PathVariable String id) throws Exception {
        return ResponseEntity.ok(_employeeService.deleteEmployee(id));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteListOfEmployee(@RequestParam List<String> ids) throws Exception {
        return ResponseEntity.ok(_employeeService.deleteEmployee(ids));
    }
}
