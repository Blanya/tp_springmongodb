package com.example.sp_mongodb_employee.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeResponseDTO {
    private String id;
    private String firstName;
    private String lastName;
    private String poste;
    private Date startingDate;
    private double salary;
}
