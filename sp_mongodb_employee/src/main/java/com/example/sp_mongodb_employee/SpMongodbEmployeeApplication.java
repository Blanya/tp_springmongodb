package com.example.sp_mongodb_employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpMongodbEmployeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpMongodbEmployeeApplication.class, args);
    }

}
