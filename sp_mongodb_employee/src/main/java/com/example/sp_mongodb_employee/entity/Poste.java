package com.example.sp_mongodb_employee.entity;

public enum Poste {
    CEO("CEO"),
    SoftwareEngineer("Software Engineer"),
    SeniorJavascriptDeveloper("Senior Javascript Developer"),
    IntegrationSpecialist("Integration Specialist");

    private String naming ;

    private Poste(String naming) {
        this.naming = naming ;
    }

    public String getNaming() {
        return  this.naming ;
    }
    }
