package com.example.sp_mongodb_employee.tools;

import com.example.sp_mongodb_employee.dto.EmployeeResponseDTO;
import com.example.sp_mongodb_employee.entity.Employee;
import org.springframework.stereotype.Component;

@Component
public class Mapper {
    //Utiliser le builder pour obtenir le DTO => pour l'intitulé du post
    public EmployeeResponseDTO fromEmployee(Employee employee){
        EmployeeResponseDTO employeeResponseDTO = EmployeeResponseDTO.builder()
                .id(employee.getId())
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .poste(employee.getPoste().getNaming())
                .salary(employee.getSalary())
                .startingDate(employee.getStartingDate()).build();
        return employeeResponseDTO;
    }
}
