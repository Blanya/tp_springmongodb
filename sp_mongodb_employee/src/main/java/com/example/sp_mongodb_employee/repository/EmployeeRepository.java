package com.example.sp_mongodb_employee.repository;

import com.example.sp_mongodb_employee.entity.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String> {
    //Pour supprimer tous les employés sélectionnés gràace à leurs ids2+
    void deleteAllByIdIn(List<String> id);
}
