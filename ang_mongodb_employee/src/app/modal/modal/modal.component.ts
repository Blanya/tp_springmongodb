import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Poste, Employee } from 'src/app/model/employee';
import { EmployeeService } from 'src/app/service/employee.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Output() newTableEmployee = new EventEmitter<Employee>();

  employees : Employee;
  postesTab: any = Poste;
  employeeForm?: FormGroup;
  _posteName = Object.keys(Poste);

  ngOnInit(): void {
    this.employeeForm = new FormGroup({
      'lastName': this.formBuilder.control('', Validators.required),
      'firstName': this.formBuilder.control('', Validators.required),
      'poste': this.formBuilder.control('', Validators.required),
      'startingDate': this.formBuilder.control('', Validators.required),
      'salary': this.formBuilder.control('', Validators.required),
    })
  }  

	constructor(private modalService: NgbModal, private formBuilder: FormBuilder, private employeeService: EmployeeService) {}

  get lastName() {
    return this.employeeForm?.get('lastName');
  }
  get firstName() {
    return this.employeeForm?.get('firstName');
  }
  get poste() {
    return this.employeeForm?.get('poste');
  }
  get startingDate() {
    return this.employeeForm?.get('startingDate');
  }
  get salary() {
    return this.employeeForm?.get('salary');
  }

  submitEmployee() {
    if (this.employeeForm?.valid) {
      let employee:Employee = this.employeeForm.value;;

      this.employeeService.postEmployee(this.employeeForm.value).subscribe({
        next: (res) => console.log(res),
        error: (error) => alert(error.error.msg),
        complete: () => {this.modalService.dismissAll; this.addNewItem(employee)}
      })
    }}

    findEmployees(): void {
      this.employeeService.getAllEmployee().subscribe({
          next: (res) => { this.employees = res; }
      });
    }

    addNewItem(value: Employee) {
      this.newTableEmployee.emit(value);
    }

	open(content: any) {
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
	}

}
