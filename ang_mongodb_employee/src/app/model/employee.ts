export interface Employee {
    id?: string,
    firstName: string,
    lastName: string, 
    startingDate: Date,
    poste: Poste | any,
    salary: number
}


export enum Poste {
  CEO = "CEO",
  SoftwareEngineer = "Software Engineer",
  SeniorJavascriptDeveloper = "Senior Javascript Developer",
  IntegrationSpecialist = "Integration Specialist"
}