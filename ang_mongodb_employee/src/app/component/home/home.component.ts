import { Component, OnInit, Output } from '@angular/core';
import { Employee } from 'src/app/model/employee';
import { EmployeeService } from 'src/app/service/employee.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{

  employees: any | Employee[];
  selected : string[] = [];
  page: number = 1;
  count: number = 0;
  tableSize: number = 7;
  tableSizes: any = [3, 6, 9, 12];
  canDelete: boolean = false;

  constructor(private employeeService: EmployeeService) {}

  ngOnInit(): void {
    this.findEmployees();
  }

  findEmployees(): void {
    this.employeeService.getAllEmployee().subscribe({
        next: (res) => { this.employees = res; }
    });
  }

  checkCheckBoxvalue(event: any): void {
    //Afficher id
    const id = event.srcElement.value;
    //Ajouter ou supprimer id du tableau
    this.selected.includes(id) ? (this.selected.splice(this.selected.indexOf(id), 1)) : this.selected.push(id);
    
    this.selected.length > 0 ? this.canDelete = true : this.canDelete = false;
  }

  deleteAll(): void{
    this.employeeService.deleteManyEmployee(this.selected).subscribe({
      next: () => alert("suppression des employés ok")
    })
    //Remise a 0 du tableau des checkbox
    this.selected = [];

    //Actualiser la liste des employés
    this.findEmployees();
  }

  addItem(newItem: Employee) {
    this.employees.push(newItem);
  }

  onTableDataChange(event: any) {
    this.page = event;
    this.findEmployees();
  }
  
  onTableSizeChange(event: any): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.findEmployees();
  }
}