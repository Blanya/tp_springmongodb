import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../model/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  url:string = "http://localhost:8080/api/employee"

  constructor(private http: HttpClient) { }

  getAllEmployee(): Observable<any>
  {
    return this.http.get<any>(this.url);
  }

  getOneEmployeeById(id: any): Observable<any>
  {
    return this.http.get<Employee>(`${this.url}/${id}`);
  }

  postEmployee(employee : Employee): Observable<any>
  {
    return this.http.post<any>(this.url, employee);
  }

  updateEmployee(employee : Employee): Observable<any>
  {
    return this.http.put<any>(this.url, employee);
  }

  deleteEmployee(id: string): Observable<any>
  {
    return this.http.delete<any>(`${this.url}/${id}`)
  }

  deleteManyEmployee(ids: Array<string>): Observable<any>
  {
    return this.http.delete<any>(`${this.url}/delete`, {params: {ids: ids}})
  }
}
